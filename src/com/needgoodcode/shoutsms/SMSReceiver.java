package com.needgoodcode.shoutsms;

import org.apache.commons.lang3.StringUtils;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.media.AudioManager;
import android.os.Bundle;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.telephony.SmsMessage;
import android.text.TextUtils;
import android.util.Log;

public class SMSReceiver extends BroadcastReceiver {

    private static final String TAG = "SMSReceiver";

    private static final double TRIGGER_PERCENTAGE = 0.15;

    private static final int RESET_DELAY = 2000;
    private static final Handler sResetHandler = new Handler();
    private static ResetSettingsRunnable sResetRunnable;

    @Override
    public void onReceive(Context context, Intent intent) {
        String key = context.getString(R.string.pref_key_volume);
        boolean enabled = PreferenceManager.getDefaultSharedPreferences(context).getBoolean(key, false);

        if (!enabled) {
            return;
        }

        Bundle bundle = intent.getExtras();
        if (bundle != null) {
            Object[] smsextras = (Object[]) bundle.get("pdus");

            for (int i = 0; i < smsextras.length; i++) {
                // parse message
                SmsMessage message = SmsMessage.createFromPdu((byte[]) smsextras[i]);
                
                // check urgency
                String body = message.getMessageBody();
                if (!TextUtils.isEmpty(body)) {
                    Log.d(TAG, "message received: " + body);
                    if (isMessageUrgent(context, body)) {
                        setRingerVolume(context);
                        break;
                    }
                }
            }
        }
    }

    private boolean isMessageUrgent(Context context, String message) {
        // all caps
        if (StringUtils.isAllUpperCase(getRawMessage(message))) {
            Log.d(TAG, "all uppercase");
            return true;
        }

        // consecutive exclamations
        if (doesMessageContainConsecutiveSubstring(message, "!")) {
            Log.d(TAG, "consecutive exclamations");
            return true;
        }

        // instance of any trigger words that occupy 
        // a significant percentage of the total message
        String[] triggers = context.getResources().getStringArray(R.array.trigger_words);
        for (String trigger : triggers) {
            if (isTriggerSignificant(message, trigger)) {
                return true;
            }
        }

        return false;
    }

    private String getRawMessage(String message) {
        return message.replaceAll("[^a-zA-Z]", "");
    }

    private double getTriggerPercentage(String message, String trigger) {
        return trigger.length() / (message.length() * 1.0);
    }
    
    private boolean isTriggerSignificant(String message, String trigger) {
        return StringUtils.containsIgnoreCase(message, trigger) && getTriggerPercentage(message, trigger) >= TRIGGER_PERCENTAGE;
    }
    
    private boolean doesMessageContainConsecutiveSubstring(String message, String substring) {
        int index = message.indexOf(substring);
        while (index >= 0) {
            int lastIndex = index;
            index = message.indexOf(substring, index + 1);

            if (index - 1 == lastIndex) {
                return true;
            }
        }
        
        return false;
    }

    private void setRingerVolume(Context context) {
        if (sResetRunnable != null) {
            sResetHandler.removeCallbacks(sResetRunnable);
        }

        AudioManager audioManager = (AudioManager) context.getSystemService(Context.AUDIO_SERVICE);
        
        // store current values
        final int previousRingerMode = audioManager.getRingerMode();
        final int previousVolume = audioManager.getStreamVolume(AudioManager.STREAM_RING);

        // bring the noise
        int maxVolume = audioManager.getStreamMaxVolume(AudioManager.STREAM_RING);
        audioManager.setRingerMode(AudioManager.RINGER_MODE_NORMAL);
        audioManager.setStreamVolume(AudioManager.STREAM_RING, maxVolume, 0);

        // reset volume after sound is played
        sResetRunnable = new ResetSettingsRunnable(audioManager, previousRingerMode, previousVolume);
        sResetHandler.postDelayed(sResetRunnable, RESET_DELAY);
    }

    public static class ResetSettingsRunnable implements Runnable {
        private AudioManager mAudioManager;
        private int mRingerMode;
        private int mVolume;

        public ResetSettingsRunnable(AudioManager audioManager, int ringerMode, int volume) {
            mAudioManager = audioManager;
            mRingerMode = ringerMode;
            mVolume = volume;
        }

        @Override
        public void run() {
            Log.d(TAG, "resetting settings");
            mAudioManager.setRingerMode(mRingerMode);
            mAudioManager.setStreamVolume(AudioManager.STREAM_RING, mVolume, 0);
        }
    }

}
